<?php

use App\Http\Controllers\DaftarController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/daftar', [DaftarController::class, 'daftar'])->name('daftar');
Route::post('/daftarPost', [DaftarController::class, 'daftar_post'])->name('daftarPost');
Route::get('/daftar/hasil/print/{nis}', [DaftarController::class, 'daftar_print'])->name('daftarPrint');

Auth::routes([
    'register' => false, // Registration Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('admin/home', [HomeController::class, 'adminHome'])->name('admin.home')->middleware('is_admin');
Route::get('/daftar/hasil/edit/{id}', [DaftarController::class, 'daftar_edit'])->middleware('auth')->name('daftarEdit');
Route::put('/daftarUpdate/{id}', [DaftarController::class, 'daftar_update'])->middleware('auth')->name('daftarUpdate');
Route::post('/daftarVerif', [HomeController::class, 'siswa_verif'])->middleware('auth')->name('siswaVerif');
Route::get('/home/verif', [HomeController::class, 'siswa_verified'])->middleware('auth')->name('siswavVerified');
Route::delete('/verifDelete/{id}', [HomeController::class, 'siswa_delete'])->middleware('is_admin')->name('siswaDelete');

