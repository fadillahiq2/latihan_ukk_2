<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\SiswaVerif;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $siswa = User::with('siswa')->get();

        return view('home', compact('siswa'));
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        $siswas = SiswaVerif::latest()->paginate(10);
        return view('adminHome', compact('siswas'))->with('i');
    }

    public function siswa_verified() {
        $siswa = User::with('siswa')->get();
        return view('siswa-verif', compact('siswa'));
    }

     public function siswa_verif(Request $request) {
         $user = DB::table('siswa_verifs')->where('nis',$request->nis)->first();
         $request->validate([
             'nis' => 'required|numeric',
             'email' => 'required|email',
             'nama' => 'required|max:50',
             'jenkel' => 'required',
             'temp_lahir' => 'required',
             'tgl_lahir' => 'required',
             'alamat' => 'required',
             'asal_sekolah' => 'required',
             'kelas' => 'required',
             'jurusan' => 'required'
         ]);

        if(!$user){

            SiswaVerif::create($request->all());

            return redirect()->route('siswavVerified')->with('success', 'Data Berhasil Diverifikasi !');

        }else if($user){
            return redirect()->route('siswavVerified')->with('error', 'Data Sudah Pernah Diverifikasi !!!');
        }


    }

    public function siswa_delete($id) {
        $siswa = SiswaVerif::findOrFail($id);
        $siswa->delete();

        return redirect()->route('admin.home')->with('success', 'Data Berhasil Dihapus');
    }



}
