@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                    <form action="{{ route('daftarUpdate',Auth::user()->siswa->nis) }}" method="POST" id="myForm">
                        @csrf
                        @method('PUT')
                    <div class="row">
                        <div class="form-group col-md-6">
                          <label class="form-label" for="nis">NIS</label>
                          <input type="number" name="nis" required id="nis" value="{{ Auth::user()->siswa->nis }}" class="form-control" placeholder="Masukkan NIS Anda" />
                        </div>
                        <div class="form-group col-md-6">
                          <label class="form-label" for="nama">Nama Lengkap</label>
                          <input
                            type="text" name="nama" id="nama" value="{{ Auth::user()->name }}" class="form-control" placeholder="Masukkan Nama Anda" required />
                        </div>
                      </div>
                      <div class="row mt-3">
                          <div class="form-group col-md-4">
                              <label class="form-label" for="jenkel">Jenis Kelamin</label>
                              <select class="form-control" name="jenkel" id="jenkel" required>
                                <option value="Laki-Laki" @if(Auth::user()->siswa->jenkel=='Laki-Laki') selected='selected' @endif>Laki-Laki</option>
                                <option value="Perempuan" @if(Auth::user()->siswa->jenkel=='Perempuan') selected='selected' @endif>Perempuan</option>
                              </select>
                          </div>

                          <div class="form-group col-md-4">
                            <label class="form-label" for="temp_lahir">Tempat Lahir</label>
                            <input type="text" name="temp_lahir" value="{{ Auth::user()->siswa->temp_lahir }}" id="temp_lahir" class="form-control" placeholder="Masukkan Tempat Lahir Anda" required/>
                          </div>
                          <div class="form-group col-md-4">
                              <label class="form-label" for="tgl_lahir">Tanggal Lahir</label>
                              <input type="text" name="tgl_lahir" value="{{ Auth::user()->siswa->tgl_lahir }}" id="tgl_lahir" class="form-control" required/>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-12">
                              <label class="form-label" for="alamat">Alamat</label>
                              <textarea class="form-control" name="alamat" required placeholder="Masukkan Alamat Anda" id="alamat" cols="30" rows="5">{{ Auth::user()->siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="asal_sekolah">Asal Sekolah</label>
                              <input placeholder="Masukkan Asal Sekolah Anda" type="text" value="{{ Auth::user()->siswa->asal_sekolah }}" name="asal_sekolah" id="asal_sekolah" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="form-label" for="kelas">Kelas</label>
                              <input placeholder="Masukkan Kelas Anda" type="text" value="{{ Auth::user()->siswa->kelas }}" name="kelas" id="kelas" class="form-control" required/>
                          </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="jurusan">Jurusan</label>
                              <select class="form-control" name="jurusan" id="jurusan" required>
                                        <option value="Otomatisasi Tata Kelola Perkantoran" @if(Auth::user()->siswa->jurusan=='Otomatisasi Tata Kelola Perkantoran') selected='selected' @endif>Otomatisasi Tata Kelola Perkantoran</option>
                                        <option value="Multimedia" @if(Auth::user()->siswa->jurusan=='Multimedia') selected='selected' @endif>Multimedia</option>
                                        <option value="Rekayasa Perangkat Lunak" @if(Auth::user()->siswa->jurusan=='Rekayasa Perangkat Lunak') selected='selected' @endif>Rekayasa Perangkat Lunak</option>
                                        <option value="Bisnis Daring dan Pemasaran" @if(Auth::user()->siswa->jurusan=='Bisnis Daring dan Pemasaran') selected='selected' @endif>Bisnis Daring dan Pemasaran</option>
                                        <option value="Teknik Komputer dan Jaringan" @if(Auth::user()->siswa->jurusan=='Teknik Komputer dan Jaringan') selected='selected' @endif>Teknik Komputer dan Jaringan</option>
                                        <option value="Perhotelan" @if(Auth::user()->siswa->jurusan=='Perhotelan') selected='selected' @endif>Perhotelan</option>
                                        <option value="Tataboga" @if(Auth::user()->siswa->jurusan=='Tataboga') selected='selected' @endif>Tataboga</option>
                              </select>
                            </div>
                        </div>
                        <br>
                              <button class="btn btn-warning" type="submit" >Perbarui</button>
                              <a class="btn btn-primary" href="{{ url()->previous() }}">Kembali</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
