@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-label" for="nis">NIS</label>
                          <input type="number" name="nis" disabled required id="nis" value="{{ Auth::user()->siswa->nis }}" class="form-control" placeholder="Masukkan NIS Anda" />
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-label" for="nis">Email</label>
                            <input type="email" name="email" required id="email" class="form-control" placeholder="Masukkan Email Anda"  value="{{ Auth::user()->email }}"/>
                          </div>
                        <div class="form-group col-md-4">
                          <label class="form-label" for="nama">Nama Lengkap</label>
                          <input
                            type="text" name="nama" id="nama" disabled value="{{ Auth::user()->name }}" class="form-control" placeholder="Masukkan Nama Anda" required />
                        </div>
                      </div>
                      <div class="row mt-3">
                          <div class="form-group col-md-4">
                              <label class="form-label" for="jenkel">Jenis Kelamin</label>
                              <select class="form-control" disabled name="jenkel" id="jenkel" required>
                                <option value="{{ Auth::user()->siswa->jenkel }}" disabled selected>{{ Auth::user()->siswa->jenkel }}</option>
                              </select>
                          </div>

                          <div class="form-group col-md-4">
                            <label class="form-label" for="temp_lahir">Tempat Lahir</label>
                            <input type="text" name="temp_lahir" disabled value="{{ Auth::user()->siswa->temp_lahir }}" id="temp_lahir" class="form-control" placeholder="Masukkan Tempat Lahir Anda" required/>
                          </div>
                          <div class="form-group col-md-4">
                              <label class="form-label" for="tgl_lahir">Tanggal Lahir</label>
                              <input type="text" name="tgl_lahir" disabled value="{{ Auth::user()->siswa->tgl_lahir }}" id="tgl_lahir" class="form-control" required/>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-12">
                              <label class="form-label" for="alamat">Alamat</label>
                              <textarea class="form-control" name="alamat" disabled required placeholder="Masukkan Alamat Anda" id="alamat" cols="30" rows="5">{{ Auth::user()->siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="asal_sekolah">Asal Sekolah</label>
                              <input placeholder="Masukkan Asal Sekolah Anda" disabled type="text" value="{{ Auth::user()->siswa->asal_sekolah }}" name="asal_sekolah" id="asal_sekolah" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="form-label" for="kelas">Kelas</label>
                              <input placeholder="Masukkan Kelas Anda" type="text" disabled value="{{ Auth::user()->siswa->kelas }}" name="kelas" id="kelas" class="form-control" required/>
                          </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="jurusan">Jurusan</label>
                              <select class="form-control" disabled name="jurusan" id="jurusan" required>
                                  <option value="{{ Auth::user()->jurusan }}" selected disabled>{{ Auth::user()->siswa->jurusan }}</option>
                                </select>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
