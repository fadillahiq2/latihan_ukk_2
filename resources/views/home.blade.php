@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @if ($message = Session::get('error'))
                    <div class="alert alert-danger">
                        <p>{{ $message }}</p>
                    </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('siswaVerif') }}" method="POST" id="my-form">
                        @csrf
                    <div class="row">
                        <div class="form-group col-md-4">
                          <label class="form-label" for="nis">NIS</label>
                          <input type="number" name="nis" readonly required id="nis" @error('nis') is-invalid @enderror" value="{{ Auth::user()->siswa->nis }}" class="form-control" placeholder="Masukkan NIS Anda" />
                          @error('nis')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                        <div class="form-group col-md-4">
                            <label class="form-label" for="email">Email</label>
                            <input type="email" name="email" readonly required id="email" @error('email') is-invalid @enderror" value="{{ Auth::user()->email }}" class="form-control" placeholder="Masukkan Email Anda" />
                            @error('email')
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                              </span>
                            @enderror
                          </div>
                        <div class="form-group col-md-4">
                          <label class="form-label" for="nama">Nama Lengkap</label>
                          <input type="text" name="nama" id="nama" @error('nama') is-invalid @enderror" readonly value="{{ Auth::user()->name }}" class="form-control" placeholder="Masukkan Nama Anda" required />
                          @error('nama')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                          @enderror
                        </div>
                      </div>
                      <div class="row mt-3">
                          <div class="form-group col-md-4">
                              <label class="form-label" for="jenkel">Jenis Kelamin</label>
                              <select class="form-control" readonly name="jenkel" id="jenkel" required>
                                <option value="{{ Auth::user()->siswa->jenkel }}" readonly selected>{{ Auth::user()->siswa->jenkel }}</option>
                              </select>
                          </div>

                          <div class="form-group col-md-4">
                            <label class="form-label" for="temp_lahir">Tempat Lahir</label>
                            <input type="text" name="temp_lahir" readonly value="{{ Auth::user()->siswa->temp_lahir }}" id="temp_lahir" class="form-control" placeholder="Masukkan Tempat Lahir Anda" required/>
                          </div>
                          <div class="form-group col-md-4">
                              <label class="form-label" for="tgl_lahir">Tanggal Lahir</label>
                              <input type="text" name="tgl_lahir" readonly value="{{ Auth::user()->siswa->tgl_lahir }}" id="tgl_lahir" class="form-control" required/>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-12">
                              <label class="form-label" for="alamat">Alamat</label>
                              <textarea class="form-control" name="alamat" readonly required placeholder="Masukkan Alamat Anda" id="alamat" cols="30" rows="5">{{ Auth::user()->siswa->alamat }}</textarea>
                            </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="asal_sekolah">Asal Sekolah</label>
                              <input placeholder="Masukkan Asal Sekolah Anda" readonly type="text" value="{{ Auth::user()->siswa->asal_sekolah }}" name="asal_sekolah" id="asal_sekolah" class="form-control" required/>
                          </div>
                          <div class="form-group col-md-6">
                              <label class="form-label" for="kelas">Kelas</label>
                              <input placeholder="Masukkan Kelas Anda" type="text" readonly value="{{ Auth::user()->siswa->kelas }}" name="kelas" id="kelas" class="form-control" required/>
                          </div>
                        </div>
                        <div class="row mt-3">
                          <div class="form-group col-md-6">
                              <label class="form-label" for="jurusan">Jurusan</label>
                              <select class="form-control" readonly name="jurusan" id="jurusan" required>
                                <option value="{{ Auth::user()->siswa->jurusan }}" readonly selected>{{ Auth::user()->siswa->jurusan }}</option>
                              </select>
                            </div>
                        </div>
                        <br>
                              <a class="btn btn-warning" href="{{ route('daftarEdit',Auth::user()->siswa->nis) }}" >Edit</a>
                              <button class="btn btn-success" id="btn-submit" type="submit">Verifikasi Data</button>
                              {{-- <a class="btn btn-success" href="{{ route('daftarPrint',Auth::user()->siswa->nis) }}">Print</a> --}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



